# projectS : Script to run at nodes
# To publish panel data to network

import time, sys, signal, atexit
import datetime

# Hardware controls
import mraa

# MQTT Lib
import paho.mqtt.client as mqtt

# Date time funtions
import datetime as libdt

# For calculating Sun's positions
from Pysolar.solar import *

PANEL_ID = 1

POST_INTERVAL	=  5.0
UPDATE_INTERVAL	= 10.0
# {lat : 12.9670459, lng : 77.6142883}
# {lat : 12.9775845, lng : 77.6097822}
# {lat : 12.9755354, lng : 77.6072717}

panelPositions = [
				 	{ 'lat' : 12.9670459, 'lng' : 77.6142883 },
				 	{ 'lat' : 12.9775845, 'lng' : 77.6097822 },
				 	{ 'lat' : 12.9755354, 'lng' : 77.6072717 },
				 	{ 'lat' : 12.9755534, 'lng' : 77.6077217 }
				 ]


# Topics for client
publishTopic	= "private/solar/area1/panel1"
publishTopic1	= "private/solar/area1/panel2"
publishTopic2	= "private/solar/area1/panel3"
publishTopic3	= "private/solar/area1/panel4"
subscribeTopic	= "private/solar/area1/#"

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

def onConnect( mqttc, obj, flags, rc ):
	print( "rc: " + str(rc) )

def onMessage( mqttc, obj, msg ):
	if msg.topic != publishTopic:	# Ignore self messages
	print( "New Message: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload) )

def onPublish( mqttc, obj, mid ):
	print( "Publish success for msg#" + str(mid) )

def onSubscribe( mqttc, obj, mid, granted_qos ):
	print( "Subscribed: " + str(mid) + " with QoS " + str(granted_qos) )

def onLog( mqttc, obj, level, string ):
	print( string )

print( "Using mraa : %s" % mraa.getVersion() )

try:
	dataFile = open( "panelData_20160317_1800.txt", "a" )
except:
	print( "Filed to open output file" )
	sys.exit( 0 )

# Configure MQTT client
mqttClient	= mqtt.Client()
mqttClient.on_connect	= onConnect
mqttClient.on_message 	= onMessage
mqttClient.on_publish	= onPublish
mqttClient.on_subscribe = onSubscribe

# Connect to broker
mqttClient.connect( "192.168.20.222", 1883 )
mqttClient.subscribe( subscribeTopic )

try:
	# Open ports
	panel0 = mraa.Aio( 0 );
	panel1 = mraa.Aio( 1 );
	panel2 = mraa.Aio( 2 );
	panel3 = mraa.Aio( 3 );

	dt = libdt.datetime.now();
	p0Alt = GetAltitude( panelPositions[0]['lat'], panelPositions[0]['lng'], dt );
	p0Azi = GetAzimuth(  panelPositions[0]['lat'], panelPositions[0]['lng'], dt );
	p1Alt = GetAltitude( panelPositions[1]['lat'], panelPositions[1]['lng'], dt );
	p1Azi = GetAzimuth(  panelPositions[1]['lat'], panelPositions[1]['lng'], dt );
	p2Alt = GetAltitude( panelPositions[2]['lat'], panelPositions[2]['lng'], dt );
	p2Azi = GetAzimuth(  panelPositions[2]['lat'], panelPositions[2]['lng'], dt );
	p3Alt = GetAltitude( panelPositions[3]['lat'], panelPositions[3]['lng'], dt );
	p3Azi = GetAzimuth(  panelPositions[3]['lat'], panelPositions[3]['lng'], dt );
	
	counter = 0;
	delayCounter = 0;

	# Start MQTT background processing
	mqttClient.loop_start();

	while True:
		print "Counter: %d" %(counter)
		# Take time and caculate sun position
		if counter*POST_INTERVAL >= UPDATE_INTERVAL:	# Wait for DELAY
			print "Updating configuration"
			dt = libdt.datetime.now();
			p0Alt = GetAltitude( panelPositions[0]['lat'], panelPositions[0]['lng'], dt );
			p0Azi = GetAzimuth(  panelPositions[0]['lat'], panelPositions[0]['lng'], dt );

			p1Alt = GetAltitude( panelPositions[1]['lat'], panelPositions[1]['lng'], dt );
			p1Azi = GetAzimuth(  panelPositions[1]['lat'], panelPositions[1]['lng'], dt );

			p2Alt = GetAltitude( panelPositions[2]['lat'], panelPositions[2]['lng'], dt );
			p2Azi = GetAzimuth(  panelPositions[2]['lat'], panelPositions[2]['lng'], dt );

			p3Alt = GetAltitude( panelPositions[3]['lat'], panelPositions[3]['lng'], dt );
			p3Azi = GetAzimuth(  panelPositions[3]['lat'], panelPositions[3]['lng'], dt );

		# Get measurements
		p0ValRaw = panel0.readFloat();
		p1ValRaw = panel1.readFloat();
		p2ValRaw = panel2.readFloat();
		p3ValRaw = panel3.readFloat();

		p0VolPhy = p0ValRaw * (1000+680+680)/(680) * 5
		p1VolPhy = p1ValRaw * (1000+680+680)/(680) * 5
		p2VolPhy = p2ValRaw * (1000+680+680)/(680) * 5
		p3VolPhy = p3ValRaw * (1000+680+680)/(680) * 5

		# Dump a copy to file
		dataLine = "%7.4fV %7.4fV" %(p0VolPhy, p1VolPhy)
		timeStamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
		fileBuffer = "%s %s\n" %( timeStamp, dataLine )
		print fileBuffer,
		dataFile.write( fileBuffer );

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(p0VolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( p0Alt ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( p0Azi ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( counter ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic, msgPacket )

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(p1VolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( p1Alt ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( p1Azi ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( counter ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic1, msgPacket )

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(p2VolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( p2Alt ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( p2Azi ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( counter ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic2, msgPacket )

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(p3VolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( p3Alt ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( p3Azi ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( counter ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic3, msgPacket )

		counter += 1
		if( counter > 3):
			counter = 0
		# Sleep a little
		time.sleep( POST_INTERVAL );
except Exception, e:
	print( str(e) );

mqttClient.loop_stop()
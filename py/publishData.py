# Surya data publish code

import time, sys, signal, atexit
import datetime
import paho.mqtt.client as mqtt

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

def onConnect( mqttc, obj, flags, rc ):
	print( "rc: " + str(rc) )

def onMessage( mqttc, obj, msg ):
	print( "New Message: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload) )

def onPublish( mqttc, obj, mid ):
	print( "Publish success for msg#" + str(mid) )

def onSubscribe( mqttc, obj, mid, granted_qos ):
	print( "Subscribed: " + str(mid) + " with QoS " + str(granted_qos) )

def onLog( mqttc, obj, level, string ):
	print( string )

# Register exit handlers
atexit.register( exitHandler )
signal.signal( signal.SIGINT, SIGINTHandler )

# Topics to publish
publishTopic	= "private/test/presence/node1"
subscribeTopic	= "private/test/#"

# Create client
mqttClient	= mqtt.Client()
mqttClient.on_connect	= onConnect
mqttClient.on_message 	= onMessage
mqttClient.on_publish	= onPublish
mqttClient.on_subscribe = onSubscribe

# Connect to broker
mqttClient.connect( "192.168.1.220", 1883 )
mqttClient.subscribe( subscribeTopic )

# Start mqtt thread
mqttClient.loop_start()
# Logic goes here
try:
	while True:
		timeStamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
		msgPacket = '{"timeStamp":"' + timeStamp + '"}'
		print( "Sending Packet with data: " + msgPacket )
		mqttClient.publish( publishTopic, msgPacket )
		time.sleep( 5 )
except:
	mqttClient.loop_stop()
	


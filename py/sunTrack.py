# Track sun position

import datetime as libdt
from Pysolar.solar import *

panel_latitude	=	12.34
panel_longitue	=	77.34

# Print tracjectory for all day
for hr in range( 0, 24 ):
	for minute in range(0,60,15):
		dt = libdt.datetime( 2016, 03, 20, hr, minute )
		# print hr, ":", minute
		currAltitude = GetAltitude( panel_latitude, panel_longitue, dt )
		currAzimuth	 = GetAzimuth( panel_latitude, panel_longitue, dt )
		print "%02d:%02d:00    %6.2f    %6.2f" %( hr, minute, currAltitude, currAzimuth )
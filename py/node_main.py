# projectS : Script to run at nodes
# To publish panel data to network

import time, sys, signal, atexit
import datetime
import random
import json

# Hardware controls
import mraa

# MQTT Lib
import paho.mqtt.client as mqtt

# Date time funtions
import datetime as libdt

# For calculating Sun's positions
from Pysolar.solar import *

PANEL_ID 	= 4
PANEL_INPUT = 3

POST_INTERVAL	=  5.0
UPDATE_INTERVAL	= 10.0
# {lat : 12.9670459, lng : 77.6142883}
# {lat : 12.9775845, lng : 77.6097822}
# {lat : 12.9755354, lng : 77.6072717}

panelPositions = [
				 	{ 'lat' : 12.9670459, 'lng' : 77.6142883 },
				 	{ 'lat' : 12.9775845, 'lng' : 77.6097822 },
				 	{ 'lat' : 12.9755354, 'lng' : 77.6072717 },
				 	{ 'lat' : 12.9755534, 'lng' : 77.6077217 }
				 ]


# Topics for client
publishTopic	= "private/solar/area1/panel" + str( PANEL_ID )
subscribeTopic	= "private/solar/area1/#"

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

def onConnect( mqttc, obj, flags, rc ):
	print( "rc: " + str(rc) )

def onMessage( mqttc, obj, msg ):
	if msg.topic != publishTopic:	# Ignore self messages
		# print( "New Message: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload) )
		print( "New configuration from " + msg.topic )
		msgInfo = json.loads( msg.payload )
		print "\tVoltage    : ", msgInfo['panelvoltage']
		print "\tTilt Angle : ", msgInfo['tiltAngle']
		print "\tPan Angle  : ", msgInfo['panAngle']

def onPublish( mqttc, obj, mid ):
	print( "Publish success for msg#" + str(mid) )

def onSubscribe( mqttc, obj, mid, granted_qos ):
	print( "Subscribed: " + str(mid) + " with QoS " + str(granted_qos) )

def onLog( mqttc, obj, level, string ):
	print( string )

print( "Using mraa : %s" % mraa.getVersion() )

try:
	dataFile = open( "panelData_"+str(PANEL_ID) + ".txt", "a" )
except:
	print( "Filed to open output file" )
	sys.exit( 0 )

# Configure MQTT client
mqttClient	= mqtt.Client()
mqttClient.on_connect	= onConnect
mqttClient.on_message 	= onMessage
mqttClient.on_publish	= onPublish
mqttClient.on_subscribe = onSubscribe

# Connect to broker
mqttClient.connect( "192.168.20.222", 1883 )
mqttClient.subscribe( subscribeTopic )

try:
	# Open ports
	panelInput = mraa.Aio( PANEL_INPUT );
	
	dt = libdt.datetime.now();
	lastUpdated = dt;
	currAlt = GetAltitude( panelPositions[PANEL_ID-1]['lat'], panelPositions[PANEL_ID-1]['lng'], dt );
	currAzi = GetAzimuth(  panelPositions[PANEL_ID-1]['lat'], panelPositions[PANEL_ID-1]['lng'], dt );
	
	counter = 0;
	delayCounter = 0;

	# Start MQTT background processing
	mqttClient.loop_start();

	while True:
		# Take time and caculate sun position
		if ( (libdt.datetime.now()-lastUpdated).seconds >= UPDATE_INTERVAL ):	# 
			print "Updating configuration .... "
			dt = libdt.datetime.now();
			newAlt = GetAltitude( panelPositions[PANEL_ID-1]['lat'], panelPositions[PANEL_ID-1]['lng'], dt );
			newAzi = GetAzimuth(  panelPositions[PANEL_ID-1]['lat'], panelPositions[PANEL_ID-1]['lng'], dt );

			# Do a little randomization
			if( random.random() < 0.30 ):	# Try a new configuration
				print "Trying calculated configuration"
				currAlt = newAlt;
				currAzi = newAzi;
			else:	# Try calculated configuration
				print "Trying a peer's configuration"
				currAlt = newAlt;
				currAzi = newAzi;

			#TODO : Update to servo


		# Get measurements
		panelValRaw = panelInput.readFloat();
		panelVolPhy = panelValRaw * (1000+680+680)/(680) * 5
		
		# Dump a copy to file
		dataLine = "%7.4fV" %(panelVolPhy)
		timeStamp = datetime.datetime.strftime( datetime.datetime.now(), '%Y-%m-%d %H:%M:%S' )
		fileBuffer = "%s %s\n" %( timeStamp, dataLine )
		print fileBuffer,
		dataFile.write( fileBuffer );

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(panelVolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( currAlt ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( currAzi ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( counter ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic, msgPacket )

		counter += 1
		if( counter > 3):
			counter = 0
		# Sleep a little
		time.sleep( POST_INTERVAL );
except Exception, e:
	print( str(e) );

mqttClient.loop_stop()
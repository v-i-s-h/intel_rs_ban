# projectS : Script to run at nodes
# To publish panel data to network

import time, sys, signal, atexit
import datetime

# Hardware controls
import mraa

# MQTT Lib
import paho.mqtt.client as mqtt

# Topics for client
publishTopic	= "private/solar/area1/panel1"
subscribeTopic	= "private/solar/area1/#"

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

def onConnect( mqttc, obj, flags, rc ):
	print( "rc: " + str(rc) )

def onMessage( mqttc, obj, msg ):
	if msg.topic != publishTopic:	# Ignore self messages
		print( "New Message: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload) )

def onPublish( mqttc, obj, mid ):
	print( "Publish success for msg#" + str(mid) )

def onSubscribe( mqttc, obj, mid, granted_qos ):
	print( "Subscribed: " + str(mid) + " with QoS " + str(granted_qos) )

def onLog( mqttc, obj, level, string ):
	print( string )

print( "Using mraa : %s" % mraa.getVersion() )

try:
	dataFile = open( "panelData_20160317_1800.txt", "a" )
except:
	print( "Filed to open output file" )
	sys.exit( 0 )

# Configure MQTT client
mqttClient	= mqtt.Client()
mqttClient.on_connect	= onConnect
mqttClient.on_message 	= onMessage
mqttClient.on_publish	= onPublish
mqttClient.on_subscribe = onSubscribe

# Connect to broker
mqttClient.connect( "192.168.20.222", 1883 )
mqttClient.subscribe( subscribeTopic )

try:
	panel0 = mraa.Aio( 0 );
	panel1 = mraa.Aio( 1 );
	while True:
		p0ValRaw = panel0.readFloat();
		p1ValRaw = panel1.readFloat();
		p0VolPhy = p0ValRaw * (1000+680+680)/(680) * 5
		p1VolPhy = p1ValRaw * (1000+680+680)/(680) * 5

		# Dump a copy to file
		dataLine = "%7.4fV %7.4fV" %(p0VolPhy, p1VolPhy)
		timeStamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
		fileBuffer = "%s %s\n" %( timeStamp, dataLine )
		print fileBuffer,
		dataFile.write( fileBuffer );

		# Publish to network
		msgPacket = '{'
		msgPacket += '"timeStamp":"' 		+ timeStamp 	+ '",'
		msgPacket += '"panelvoltage":"' 	+ str(p0VolPhy) + '",'
		msgPacket += '"panelvoltage1":"' 	+ str(p1VolPhy) + '",'
		msgPacket += '"tiltAngle":"' 		+ str( 45.0 ) 	+ '",'
		msgPacket += '"panAngle":"' 		+ str( 0.0 ) 	+ '",'
		msgPacket += '"counter":"' 			+ str( 0 ) 		+ '"'
		msgPacket += '}'
		mqttClient.publish( publishTopic, msgPacket )

		# Sleep a little
		time.sleep( 5 );
except:
	print( "Cannot access Aio" );
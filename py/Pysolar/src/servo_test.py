# Servo motor tests
import pyupm_servo as servo
import time, sys, signal, atexit

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

# Initialise servo
panControl = servo.Servo( 9, 400, 1900 );
# panControl.setPeriod( 20000 );

print "Test starting ..."
while True:
	for angle in range( 0, 180, 10 ):
		print "Angle: ", angle
		panControl.setAngle( angle )
		time.sleep( 0.05 )
	for angle in range( 180, 0, -10 ):
		print "Angle: ", angle
		panControl.setAngle( angle )
		time.sleep( 0.05 )
print "Test End"


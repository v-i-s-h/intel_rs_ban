# LM35 Test
import time, sys, signal, atexit
import pyupm_lm35 as sensorObj

# Instantiate a LM35 on analog pin A0, with a default analog
# reference voltage of 5.0
sensor = sensorObj.LM35(0)

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

# Every half second, sample the sensor and output the temperature

while (1):
        print "Temperature:", sensor.getTemperature(), "C"
	time.sleep(.5)
# To publish panel data to network

import time, sys, signal, atexit
import datetime
import mraa

## Exit handlers ##
# This function stops python from printing a stacktrace when you hit control-C
def SIGINTHandler(signum, frame):
	raise SystemExit

# This function lets you run code on exit
def exitHandler():
	print "Exiting"
	sys.exit(0)

# Register exit handlers
atexit.register(exitHandler)
signal.signal(signal.SIGINT, SIGINTHandler)

print( "Using mraa : %s" % mraa.getVersion() )

try:
	dataFile = open( "panelData_20160317.txt", "a" )
except:
	print( "Filed to open output file" )

try:
	panel0 = mraa.Aio( 0 );
	panel1 = mraa.Aio( 1 );
	while( 1 ):
		p0ValRaw = panel0.readFloat();
		p1ValRaw = panel1.readFloat();
		p0VolPhy = p0ValRaw * (1000+680+680)/(680) * 5
		p1VolPhy = p1ValRaw * (1000+680+680)/(680) * 5
		dataLine = "%7.4fV %7.4fV" %(p0VolPhy, p1VolPhy)
		timeStamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
		fileBuffer = "%s %s\n" %( timeStamp, dataLine )
		print fileBuffer,
		dataFile.write( fileBuffer );
		time.sleep( 60 );
except:
	print( "Cannot access Aio" );